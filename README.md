# Provender 0.1.20
Provender is a server provisioning and code deployment system written entirely
in PHP.

## History
The project was started as a way to combat the lack of support for
twitter's lg/murder project on github which didn't support the latest version
of capistrano.

With that in mind, as well as wanting to work inside the same language as much as possible, provender was born.

## Shoutouts
Thanks to Zoltan for [tcz/PHPTracker](https://github.com/tcz/PHPTracker) on github. It's the bittorrent library which makes this project a reality.

## Usage
Currently, although Provender has stable releases, it's not completely feature full yet for its first major version. Usage, API and Plugin development information will be released alongside version 1.

## Docker
Provender now has an official dockerfile to use in your docker deployments. The image is stored in the docker registry as jaitaiwan/provender.

## Licence
This project uses the extremely liberal Artistic 2.0 licence. Copyright &copy; Centric Web Estate Pty Ltd. All rights reserved.
