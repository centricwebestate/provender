<?php

namespace CWE\Tests;

use PHPUnit_Framework_TestCase as TestCase;
use CWE\Provender\Plugins\CowSay\Task as cowSayTask;
use CWE\TestHelpers\TestLogger as logger;

class CowSayTest extends TestCase
{
    protected $task;

    public function setUp()
    {
        $this->class = 'CWE\Provender\Plugins\CowSay\Task';
        $this->task = new cowSayTask();
    }

    public function testInitialisesWithoutError()
    {
        new cowSayTask();
    }

    public function testBasicUsage()
    {
        $resources = [];
        $resources['sayings'] = ["test"];
        $resources['cowAscii'] = "string";
        $resources['logger'] = new logger();

        $res = $this->task->run($resources, [], []);
        assert(is_string($res));
        assert(strstr($res, "test"));
        assert(strstr($res, "string"));
    }

    public function testBasicUsage2()
    {
        $resources = [];
        $resources['sayings'] = ["test1","test2"];
        $resources['cowAscii'] = "string";
        $resources['logger'] = new logger();

        $res = $this->task->run($resources, [], []);
        assert(is_string($res));
        assert(strstr($res, "test"));
        assert(strstr($res, "string"));
    }

    /**
     * @expectedException Error
     * A missing logger will throw a null pointer exception
     */
    public function testMissingLogger()
    {
        $resources = [];
        $resources['sayings'] = ["test"];
        $resources['cowAscii'] = "string";

        $res = $this->task->run($resources, [], []);
    }

    /**
     * Tests to make sure it doesn't fail on a missing cowAscii
     */
    public function testMissingCowAscii()
    {
        $resources = [];
        $resources['sayings'] = ["test"];
        //$resources['cowAscii'] = "string";
        $resources['logger'] = new logger();

        $res = $this->task->run($resources, [], []);
        assert(is_string($res));
        assert(strstr($res, "test"));
        assert(strstr($res, "string"));
    }

    /**
     * Tests to make sure it doesn't fail on a missing sayings
     */
    public function testMissingSayings()
    {
        $resources = [];
        //$resources['sayings'] = ["test"];
        $resources['cowAscii'] = "string";
        $resources['logger'] = new logger();

        $res = $this->task->run($resources, [], []);
        assert(is_string($res));
        assert(strstr($res, "test"));
        assert(strstr($res, "string"));
    }
}
