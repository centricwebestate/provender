<?php

namespace CWE\TestHelpers;

/**
 * This class is made purely for testing. Any messages are instead passed
 * directly back as strings instead of to the console.
 */
class TestLogger
{
    public function __construct()
    {
        
    }

    public function logInfo($msg = '')
    {
        return $msg;
    }

    public function log($msg = '')
    {
        return $msg;
    }
}
