<?php

namespace CWE\Provender;

use CWE\Provender\Interfaces\TaskQueue;
use CWE\Provender\Interfaces\Task;
use CWE\Libraries\EventEmitter\Interfaces\EventEmitter;
use CWE\Libraries\EventEmitter\Event;

class Command implements TaskQueue
{
    protected $tasks = [];
    protected $eventEmitter;
    protected $help;
    protected $allowMultiple = false;
    protected $options = null;
    protected $description = '';

    public function __construct(array &$resources, $description = '')
    {
        $this->resources = $resources;
        $this->setEventEmitter($resources['eventEmitter']);
        $this->getEventEmitter()->emit(new Event('constructor', ['self' => &$this]));
        $this->description = $description;
    }

    public function addTask(Task &$task, $priority = 1)
    {
        $task->setUID(uniqid());

        // Ensures order of addition to array
        $numberPadded = str_pad(
            (string) count($this->tasks),
            4,
            "0",
            STR_PAD_LEFT
        );
        $priority = $priority . $numberPadded;

        $this->tasks []= [$priority, $task];

        return $task;
    }

    public function removeTask($taskId)
    {
        $index = 0;
        $retVal = false;
        foreach ($this->tasks as $task) {
            if (strcmp($task[1]->getUID(), $taskId) === 0) {
                $retVal = $task[1];
                unset($this->tasks[$index]);
                break;
            }

            $index++;
        }

        return $retVal;
    }

    public function run(array $options)
    {
        $this->eventEmitter->emit(
            new Event('beforeRun', [
                'options' => $options,
                'self' => &$this
            ])
        );

        $this->prioritiseTasks();
        $results = [];
        foreach ($this->tasks as $task) {
            $result = $task[1]->run($this->resources, $options, $results);
            if (!$result) {
                break;
            }
            $results []= $result;
        }

        $this->eventEmitter->emit(
            new Event('afterRun', [
                'options' => $options,
                'self' => &$this
            ])
        );

        return !$this->allowMultiple;
    }

    public function allowPassthrough()
    {
        $this->allowMultiple = true;
    }

    private function prioritiseTasks()
    {
        usort($this->tasks, function ($a, $b) {
            return $a[0] > $b[0];
        });
    }

    private function setEventEmitter(EventEmitter &$eventEmitter)
    {
        $this->eventEmitter = $eventEmitter;
    }

    private function &getEventEmitter()
    {
        return $this->eventEmitter;
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function setOptions($options)
    {
        return $this->options = $options;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getDescription()
    {
        return $this->description;
    }
}
