<?php

namespace CWE\Provender;

class ConsoleLogger
{
    protected $stdin;
    protected $stdout;
    protected $stderr;
    protected $use_color = true;

    public function __construct()
    {
        $this->stdin = fopen('php://stdin', 'r');
        $this->stdout = fopen('php://stdout', 'w+');
        $this->stderr = fopen('php://stderr', 'w+');
    }

    protected function writeStdErr($msg = '')
    {
        fwrite($this->stderr, $msg);
    }

    protected function writeStdOut($msg = '')
    {
        fwrite($this->stdout, $msg);
    }

    protected function writeStdErrLine($msg = '')
    {
        $this->writeStdErr("$msg\n");
    }

    protected function writeStdOutLine($msg = '')
    {
        $this->writeStdOut("$msg\n");
    }

    public function log($msg = '')
    {
        $this->writeStdOutLine($msg);
    }

    public function logError($msg)
    {
        if ($this->use_color) {
            $msg = "\e[0;31m$msg\e[0m";
        }

        $this->writeStdErrLine($msg);
    }

    public function logWarn($msg)
    {
        if ($this->use_color) {
            $msg = "\e[1;33m$msg\e[0m";
        }

        $this->writeStdErrLine($msg);
    }

    public function logInfo($msg)
    {
        if ($this->use_color) {
            $msg = "\e[1;34m$msg\e[0m";
        }

        $this->writeStdErrLine($msg);
    }

    public function enableColor()
    {
        $this->use_color = true;
    }

    public function disableColor()
    {
        $this->use_color = false;
    }

    /**
     * @todo Make this work
     */
    public function readStdIn()
    {
        fread($this->stdin);
    }
}
