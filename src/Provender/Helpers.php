<?php

namespace CWE\Provender;

class Helpers
{
    public static function arrayMergeRecursive(
        array $original,
        ...$arrays
    ) {

        // Begin to merge each array
        foreach ($arrays as $array) {
            // Lets go through the array keys
            // $array
            // $original
            foreach ($array as $key => $value) {
                if (!isset($original[$key])) {
                    $original[$key] = $value;
                    continue;
                }

                if (is_array($value)) {
                    $original[$key] = self::arrayMergeRecursive(
                        $original[$key],
                        $value
                    );
                    continue;
                }

                if (is_int($key)) {
                    if (!in_array($value, $original)) {
                        $original []= $value;
                    }
                    continue;
                }

                $original[$key] = $value;
            }
        }

        return $original;
    }
}
