<?php

namespace CWE\Provender\Plugins;

use CWE\Provender;
use CWE\Provender\Command;
use CWE\Provender\Plugins\CowSay\Task;
use CWE\Libraries\ObjectRex;
use GetOptionKit\OptionCollection;

class CowSay
{
    protected $resources = [];
    protected $sayings = [
        "Remember, sudo rm -rf / is the end to all your worries",
        "Yes, I can say more than one thing! I'm a bovine genus!"
      ];
    protected $cowAscii = "             \
              \   ^__^
               \  (oo)\_______
                  (__)\       )\/\
                      ||----w |
                      ||     ||";

    public function __construct(Provender &$provender)
    {
        $this->addResource('eventEmitter', $provender->getEventEmitter());
        $this->addResource('logger', $provender->getLogger());
        $this->addResource('sayings', $this->sayings);
        $this->addResource('cowAscii', $this->cowAscii);

        $provender->registerCommand(
            new ObjectRex('/^cowsay$/'),
            $this->getCommand()
        );
    }

    protected function addResource($name, $resource)
    {
        $this->resources[$name] = $resource;
    }

    protected function getResources()
    {
        return $this->resources;
    }

    protected function getCommand()
    {
        $resources = $this->getResources();
        $help = new Task($resources);

        $command = new Command($resources);
        $command->setDescription('What does the cow say?');
        $command->addTask($help);
        return $command;
    }
}
