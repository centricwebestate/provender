<?php

namespace CWE\Provender\Plugins\Help;

use CWE\Provender\Interfaces\Task;
use CWE\Libraries\EventEmitter\Event;

class HelpTask implements Task
{
    protected $eventEmitter;
    protected $logger;
    protected $config;
    protected $uid;

    public function __construct()
    {
        
    }

    public function setUID($uid)
    {
        $this->uid = $uid;
    }

    public function getUID()
    {
        return $this->uid;
    }

    public function run(array &$resources, array $options, array $results = [])
    {
        $this->eventEmitter =& $resources['eventEmitter'];
        $this->logger =& $resources['logger'];
        $this->config =& $resources['config'];

        if (isset($options[2])) {
            if (!$this->eventEmitter->emit(
                new Event("help:{$options[2]}", $options)
            )) {
                $this->logger->logError(
                    "No help found for '{$options[2]}' command"
                );
                $this->logger->logError('');
                return true;
            } else {
                return true;
            }
        }

        $this->eventEmitter->emit(new Event('help:help', $options));
        return true;
    }
}
