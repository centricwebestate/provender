<?php

namespace CWE\Provender\Plugins;

use CWE\Provender;
use CWE\Provender\Command;
use CWE\Provender\Plugins\Copy\CopyTask;
use CWE\Provender\Plugins\Copy\HelpInformation;
use CWE\Libraries\ObjectRex;
use GetOptionKit\OptionCollection;

class Copy
{
    protected $resources = [];

    public function __construct(Provender $provender)
    {
        if (!isset($provender->getConfig()['core']['disableExamplePlugin'])) {
            $this->addResource('config', $provender->getConfig());
            $this->addResource('eventEmitter', $provender->getEventEmitter());
            $this->addResource('logger', $provender->getLogger());

            $provender->registerCommand(
                new ObjectRex('/^example:copy$/'),
                $this->getCommand()
            );

            $helpinfo = new HelpInformation();
            $provender->getEventEmitter()->addListener(
                'help:example:copy',
                [$helpinfo, 'getHelp'],
                [$provender->getLogger()]
            );
        }
    }

    protected function addResource($name, $resource)
    {
        $this->resources[$name] = $resource;
    }

    protected function &getResources()
    {
        return $this->resources;
    }

    protected function getCommand()
    {
        $copy = new CopyTask($this->getResources());

        $command = new Command($this->getResources());
        $command->setDescription('Copies a file from one place to another.');
        $command->addTask($copy);
        return $command;
    }
}
