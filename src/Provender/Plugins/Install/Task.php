<?php

namespace CWE\Provender\Plugins\Install;

use CWE\Provender\Interfaces;
use CWE\Libraries\EventEmitter\Event;
use CWE\Provender\CLI;

class Task implements Interfaces\Task
{
    protected $eventEmitter;
    protected $logger;
    protected $config;
    protected $uid;

    public function __construct()
    {

    }

    public function setUID($uid)
    {
        $this->uid = $uid;
    }

    public function getUID()
    {
        return $this->uid;
    }

    public function run(array &$resources, array $options, array $results = [])
    {
        $this->logger =& $resources['logger'];
        $version = $resources['version'];
        $this->logger->logInfo(
            "Installing provender"
        );

        CLI::install();

        $this->logger->logInfo(
            "Install Complete"
        );
        $this->logger->log();
        return true;
    }
}
