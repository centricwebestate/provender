<?php

namespace CWE\Provender\Plugins\Copy;

use CWE\Provender\Interfaces\Task;
use CWE\Libraries\EventEmitter\Event;

class CopyTask implements Task
{
    protected $eventEmitter;
    protected $logger;
    protected $config;
    protected $uid;

    public function __construct()
    {
        
    }

    public function setUID($uid)
    {
        $this->uid = $uid;
    }

    public function getUID()
    {
        return $this->uid;
    }

    public function run(array &$resources, array $options, array $results = [])
    {
        $this->eventEmitter = $resources['eventEmitter'];
        $this->logger = $resources['logger'];
        $this->config = $resources['config'];
        if (isset($options[2]) && isset($options[3])) {
            $result = copy($options[2], $options[3]);
        } else {
            $this->eventEmitter->emit(new Event(
                'help:plugin:copy',
                $options
            ));
        }
    }
}
