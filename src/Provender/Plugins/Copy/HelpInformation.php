<?php

namespace CWE\Provender\Plugins\Copy;

class HelpInformation
{
    public function getHelp($event, $logger)
    {
        $logger->log();
        $logger->logInfo('Copies a file from one place to another');
        $logger->log();
        $logger->logInfo('Usage:');
        $logger->log(
            "  provender example:copy <from> <to>"
        );
        $logger->log();
        $logger->logInfo('Arguments:');
        $logger->log("  from \tThe file to copy from");
        $logger->log("  to   \tThe file to copy to");
        $logger->log();
        $logger->logInfo("Notes:");
        $logger->log(
            "  Disable by adding `disableExamplePlugin: true` to the core config"
        );
        $logger->log();
    }
}
