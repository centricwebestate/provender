<?php

namespace CWE\Provender\Interfaces;

interface Task
{
    public function setUID($uid);
    public function getUID();

    public function run(array &$resources, array $options, array $results);
}
