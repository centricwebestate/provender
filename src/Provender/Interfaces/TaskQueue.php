<?php

namespace CWE\Provender\Interfaces;

interface TaskQueue
{
    public function addTask(Task &$task, $priority = 1);
    public function removeTask($taskId);

    public function run(array $options);
}
