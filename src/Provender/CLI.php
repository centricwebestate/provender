<?php

namespace CWE\Provender;

use CWE\Provender;
use CWE\Provender\Traits\StdIO;
use GetOptionKit\OptionCollection;
use GetOptionKit\OptionParser;

class CLI extends Provender
{
    use StdIO;

    public function run()
    {
        global $argv, $argc;
        $this->preRun();
        $args = $argv;

        // find command
        $arguments = isset($argv[1]) ? $argv[1] : '';
        $commands = $this->findCommands($arguments);


        foreach ($commands as $command) {
            // parse command options
            $opt = $command->getOptions();
            $options = array_merge_recursive(
                $this->parseOptions($opt),
                $argv
            );

            // Run the command
            if ($command->run($options) === true) {
                break;
            }
        }
    }

    public static function install()
    {
        $dotFileName = PROVENDER_DOT_FOLDER;
        $initDirectory = "{$_SERVER['HOME']}/.$dotFileName";
        if (!file_exists($initDirectory)) {
            mkdir($initDirectory);
        }

        $overwrite = false;
        if (isset($_SERVER['argv'][1])) {
            $overwrite = true;
        }

        if (!file_exists("$initDirectory/default.yml") || $overwrite) {
            copy(__DIR__.'/config/config.yml', "$initDirectory/default.yml");
        }
    }

    protected function preRun()
    {
        // Load the configuration in the home dir
        $this->loadDefaultConfig();
        $this->loadHomeConfig();

        // Try and load a project specific config
        $dotFolder = $this->findDotFolder(getcwd());
        if ($dotFolder) {
            $this->loadConfig($dotFolder);
        }
        // Assign the Event Emitter
        $eventEmitter = new $this->config['core']['eventEmitter']();
        $this->setEventEmitter($eventEmitter);

        // Load the plugins
        $this->loadPlugins();
    }

    protected function loadHomeConfig()
    {
        $in = "{$_SERVER['HOME']}/.{$this->dotFolderName}";
        if (!file_exists($in)) {
            $this->getLogger()->log();
            $this->getLogger()->logWarn(
                'Provender config in home folder not found'
            );
            $this->getLogger()->logWarn(
                implode(' ', [
                    'Have you run',
                    '`provender install`',
                    'yet?'
                ])
            );
            $this->getLogger()->log();

            $in = realpath(__DIR__ . "/config/config.yml");
        }


        $configs = [$this->config];


        $this->loadConfig($in);
    }

    protected function findCommands($commandName)
    {
        $helpCommand = null;
        $possibleCommands = [];
        foreach ($this->commands as $command) {
            if ($command[0]->test($commandName)) {
                $possibleCommands []= $command[1];
            }

            if ($command[0]->test('help')) {
                $helpCommand = $command[1];
            }
        }

        if (count($possibleCommands) > 0) {
            return $possibleCommands;
        }

        return [$helpCommand];
        exit;
    }

    protected function parseOptions(OptionCollection $options = null)
    {
        global $argv;

        if ($options == null) {
            return [];
        }

        $parser = new OptionParser($options);
        $result = $parser->parse( $argv );
        return $result->toArray();
    }

    public static function findProjectFolder($in = __DIR__)
    {
        // If we have gone as far as the users home directory.
        if (strcmp($_SERVER['HOME'], $in) === 0) {
            return false;
        }

        // Look for provender $in the dir
        $dotFolder = PROVENDER_DOT_FOLDER;
        $directoryToFind = "$in/.{$dotFolder}";
        if (file_exists($directoryToFind)) {
            // Return the .provender dir we found
            return $directoryToFind;
        }

        $next = dirname($in);
        if ($next == '.' || $next == '/' || strcmp($next, $in) === 0) {
            return false; // We've gone as far as we can
        }

        return self::findProjectFolder($next);
    }

    public function getCommands()
    {
        return $this->commands;
    }

    protected function hasAttr(array $array, $attr)
    {
        if (in_array($attr, $array)) {
            return true;
        }

        return false;
    }
}
